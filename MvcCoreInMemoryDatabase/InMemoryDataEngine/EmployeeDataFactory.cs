﻿using MvcCoreInMemoryDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcCoreInMemoryDatabase.InMemoryDataEngine
{
    public class EmployeeDataFactory
    {
        public List<Employee> Employees;

        public EmployeeDataFactory()
        {
            this.Employees = LoadEmployees();
        }
        private static List<Employee> LoadEmployees()
        {
            //random values
            List<Employee> lstEmps = new List<Employee>();
            for (int i = 0; i < 10; i++)
            {
                lstEmps.Add(new Employee
                {
                    Id = 100 + i,
                    Name = "Employee " +
                    i.ToString(),
                    Salary = 1532 * (i + 1),
                    DateOfJoin = DateTime.Now.AddYears(-i)
                }
                    );
            }
            return lstEmps;
        }
    }
    //factory method to initialize list of employees
    //factory methods are always static
   
}
