﻿using Microsoft.AspNetCore.Mvc;
using MvcCoreInMemoryDatabase.InMemoryDataEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcCoreInMemoryDatabase.Controllers
{
    public class EmployeeController : Controller
    {
        //I can call the c'tor of EmployeeDataFactory
        //EmployeeDataFactory objFactory = new EmployeeDataFactory();

        private readonly EmployeeDataFactory _factory;

        public EmployeeController(EmployeeDataFactory factory)
        {
            this._factory = factory;
        }
        public IActionResult Index()
        {
            return View(this._factory.Employees);
        }
    }
}
